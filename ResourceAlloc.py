import  struct
import json
import GdbServerProxy
import subprocess
import time

ALLOC_OK = 0x10
ALLOC_OCCUPIED  = 0x11
ALLOC_ALREADY  = 0x12

disp = {
    "list_all": 0x00,
    "reserve": 0x01
}

devices = [
    {'name': "K2TV", 'deviceID': "10101", 'used': False, 'port_debug': 2345, 'port_comm': 45001, 'run_cmd': '/usr/bin/gdbserver --multi 0.0.0.0:2345'},
    {'name': "K2TV", 'deviceID': "10102", 'used': False, 'port_debug': 2346, 'port_comm': 45001, 'run_cmd': '/usr/bin/gdbserver --multi 0.0.0.0:2346'},
]



def process_server(input,gdb_proxy):
    data = json.loads(input)
    if data['func'] == disp["list_all"]:
        return list_all_device()
    if data['func'] == disp["reserve"]:
        return reserve_device(data, gdb_proxy)


def process_resp(input):
    data = json.loads(input)
    # print(data)
    return data

def run_device_cmd(device):
    device['proc'] = subprocess.Popen(device['run_cmd'], shell=True)
    print('Device started')
    time.sleep(0.01)

def reserve_device(data, gdb_proxy):
    name = data['device_name']
    if gdb_proxy == None:
        for device in devices:
            if (name == device['name']) and (device['used'] == False):
                device_num = devices.index(device)
                p_debug = device['port_debug']
                p_comm = device['port_comm']
                device['used'] = True

                run_device_cmd(device)

                data_out = {
                    'func': data['func'],
                    'resp': ALLOC_OK
                }
                out = json.dumps(data_out)
                func_response = {
                    'type': data_out['func'],
                    'p_debug': p_debug,
                    'p_comm': p_comm,
                    'device_num': device_num,
                    'comm_out': out
                }

                return func_response
        data_out = {
            'func': data['func'],
            'resp': ALLOC_OCCUPIED
        }
        out = json.dumps(data_out)
        func_response = {
            'type': data_out['func'],
            'p_debug': None,
            'p_comm': None,
            'comm_out': out
        }
        return func_response
    else:
        data_out = {
            'func': data['func'],
            'resp': ALLOC_ALREADY
        }
        out = json.dumps(data_out)
        func_response = {
            'type': data_out['func'],
            'p_debug': None,
            'p_comm': None,
            'comm_out': out
        }
        return func_response


def free(device):
    if device is not None:
        devices[device]['used'] = False


def list_all_device():
    devices_simple = []
    for dev in devices:
        devices_simple.append({'name': dev['name'], 'used': dev['used'], 'deviceID': dev['deviceID'] }   )
    data = {
        'func': disp["list_all"],
        'devices': devices_simple
    }
    out = json.dumps(data)

    func_response = {
        'type': data['func'],
        'comm_out': out
    }
    return func_response

def ask_list_all():
    data = {
        'func': disp["list_all"]
    }
    out = json.dumps(data)
    return out


def ask_reserve(name):
    data = {
        'func': disp["reserve"],
        'device_name': name
    }
    out = json.dumps(data)
    return out
