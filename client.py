import socket
import sys
import  channel_protocol
import ResourceAlloc
import socket
import sys
import traceback
from threading import Thread
import  json
import time
import selectors
import argparse

BEGGIN =  0
ASK_DEVICES = 1
RESERVE = 2
WAIT_FOR_RESERVE = 3
IDLE = 0xFF

read_resp_global = None
gdb_conn = None
sel = selectors.DefaultSelector()
kill = False


def read_packet_from_remote_server(conn, mask, channel):
    global  read_resp_global
    status_recv = channel.recv_channel_protocol()
    if status_recv > 0:
        recv = channel.parse_recv()
        if recv is not None:
            tag = recv['tag']
            if tag == 0x00:
                read_resp_global = ResourceAlloc.process_resp(recv['payload'])
            elif tag == 0x01:
                data = json.loads(recv['payload'])
                gdb_conn.sendall(data['packet'].encode())
    else:
        global kill
        kill = True

def accept_gdb(sock, mask, channel):
    conn, addr = sock.accept()  # Should be ready
    print('accepted', conn, 'from', addr)
    global  gdb_conn
    gdb_conn = conn
    sel.register(conn, selectors.EVENT_READ, data=[read_gdb,channel])

def read_gdb(conn, mask, channel):
    data = conn.recv(1000)  # Should be ready
    if data:
        # print('echoing', repr(data), 'to', conn)
        # conn.send(data)  # Hope it won't block
        data = {
            'packet': data.decode()
        }
        out = json.dumps(data)
        channel.connection.send(channel.prep_send_channel_protocol(0x01, out))
    else:
        print('closing', conn)
        sel.unregister(conn)
        conn.close()
        global  kill
        kill = True

def main():
    parser = argparse.ArgumentParser(description='Multiplexer client')
    parser.add_argument('--gport', help='gdbserver port number', default=45000)
    args = parser.parse_args()
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "127.0.0.1"
    port = 8888

    try:
        soc.connect((host, port))

    except:
        print("Connection error")
        sys.exit()

    channel = channel_protocol.ChannelProt(soc,sel)
    sel.register(soc, selectors.EVENT_READ, data=[read_packet_from_remote_server,channel])
    is_active = True
    state = BEGGIN
    while is_active:
        events = sel.select(timeout=0.2)
        # For each new event, dispatch to its handler
        for key, mask in events:
            handler = key.data[0]
            handler(key.fileobj, mask, key.data[1])

        if state == BEGGIN:
            temp = 0
            state = ASK_DEVICES
        elif state == ASK_DEVICES:
            get_all = ResourceAlloc.ask_list_all()
            soc.send(channel.prep_send_channel_protocol(0, get_all))
            state = RESERVE
        elif state == RESERVE:
            reserve = ResourceAlloc.ask_reserve("K2TV")
            soc.send(channel.prep_send_channel_protocol(0, reserve))
            state = WAIT_FOR_RESERVE
        elif state == WAIT_FOR_RESERVE:
            if read_resp_global is not None:
                if (read_resp_global['func'] == ResourceAlloc.disp['reserve']) and (read_resp_global['resp'] == ResourceAlloc.ALLOC_OK):
                    print('Device allocated')
                    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    sock.bind(('localhost', int(args.gport)))
                    sock.listen(1)
                    sel.register(sock, selectors.EVENT_READ, data=[accept_gdb,channel])
                    state = IDLE

        if kill == True:
            sel.unregister(soc)
            soc.close()
            is_active = False


if __name__ == "__main__":
    main()
