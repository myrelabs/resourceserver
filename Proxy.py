import json
import socket
import selectors
import ResourceAlloc

class Proxy:



    def __init__(self, channel, port, dev_num):
        self.port = port
        self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.dev_num = dev_num
        host = "127.0.0.1"
        try:
            self.soc.connect((host, self.port))
            channel.sel.register(self.soc, selectors.EVENT_READ, data=[self.recvFromGdbServer, channel])
        except:
            print("Connection error")

    def sendToDst(self, data):
        self.soc.send(data)

    def recvFromGdbServer(self, conn, mask, channel):
        try:
            temp = conn.recv(4096)
            if temp:
                data = {
                    'packet': temp.decode()
                }
                out = json.dumps(data)
                # print('packet gdb out')
                channel.connection.send(channel.prep_send_channel_protocol(0x01, out))
            else:
                print('close gdb_connection')
                self.shutdown(channel.sel)
                channel.sel.unregister(conn)
                conn.close()
        except ConnectionResetError:
            print('close gdb_connection')
            self.shutdown(channel.sel)
            channel.sel.unregister(conn)
            conn.close()

    def processInput(self, input):
        data = json.loads(input)
        return data['packet'].encode()

    def packToClient(self, packet):
        data = {
            'packet': packet.decode()
        }
        out = json.dumps(data)
        return out

    def shutdown(self, sel):
        self.soc.close()
        ResourceAlloc.free(self.dev_num)
        sel.unregister(self.soc)
