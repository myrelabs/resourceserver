import socket
import sys
import traceback
from threading import Thread
import channel_protocol
import ResourceAlloc
import time
import GdbServerProxy
import selectors

def main():
    start_server()


def start_server():
    host = "127.0.0.1"
    port = 8888         # arbitrary non-privileged port

    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)   # SO_REUSEADDR flag tells the kernel to reuse a local socket in TIME_WAIT state, without waiting for its natural timeout to expire
    print("Socket created")

    try:
        soc.bind((host, port))
    except:
        print("Bind failed. Error : " + str(sys.exc_info()))
        sys.exit()

    soc.listen(5)       # queue up to 5 requests
    print("Socket now listening")

    # infinite loop- do not reset for every requests
    while True:
        connection, address = soc.accept()
        ip, port = str(address[0]), str(address[1])
        print("Connected with " + ip + ":" + port)

        try:
            Thread(target=client_thread, args=(connection, ip, port)).start()
        except:
            print("Thread did not start.")
            traceback.print_exc()

    soc.close()



def read_packet_from_remote_client(conn, mask, channel):
    status_recv = channel.recv_channel_protocol()
    if status_recv > 0:
        recv = channel.parse_recv()
        if recv is not None:
            tag = recv['tag']
            if tag == 0x00:  # ResourceAlloc
                output = ResourceAlloc.process_server(recv['payload'], channel.gdb_proxy)
                if output['type'] == ResourceAlloc.disp['reserve']:
                    channel.gdb_proxy = GdbServerProxy.GdbServerProxy(channel, output['p_debug'], output['device_num'])
                conn.sendall(channel.prep_send_channel_protocol(tag, output['comm_out']))
            elif tag == 0x01:
                 if channel.gdb_proxy is not None:
                     # print('proxy_packet income')
                     packet = channel.gdb_proxy.processInput(recv['payload'])
                     channel.gdb_proxy.sendToDst(packet)
    else:
        channel.gdb_proxy.shutdown(channel.sel)
        channel.gdb_proxy = None






def client_thread(connection, ip, port):
    is_active = True
    port_debug = 0
    port_comm = 0
    device_num = 0
    gdb_proxy = None
    sel = selectors.DefaultSelector()
    channel = channel_protocol.ChannelProt(connection, sel)
    channel.gdb_proxy = gdb_proxy
    sel.register(connection, selectors.EVENT_READ, data=[read_packet_from_remote_client, channel])
    while is_active:
        events = sel.select(timeout=0.2)

        # For each new event, dispatch to its handler
        for key, mask in events:
            handler = key.data[0]
            handler(key.fileobj, mask, key.data[1])





if __name__ == "__main__":
    main()

