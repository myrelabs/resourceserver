import  struct
import time

ACK = b'0xA0'
OK  = b'0xA1'
FAIL= b'0xF0'

timeout_ns = 30000000
header_length = 5

class ChannelProt:
    def __init__(self, connection, sel):
        self.connection = connection
        self.sel = sel
        self.buffer = b''
        self.tag = -1
        self.length = 0
        self.gdb_proxy = None


    def parse_recv(self):
        if len(self.buffer) >= header_length:
                # print(self.buffer)
                self.tag, self.length = struct.unpack('>BI', self.buffer[:header_length])
                if len(self.buffer) >= header_length + self.length:
                    ret = {'tag': self.tag, 'length': self.length, 'payload': self.buffer[header_length:].decode("utf-8")}
                    self.buffer = b''
                    self.tag = -1
                    self.length = 0
                    return ret

                else:
                    return None
        else:
            return None


    def recv_channel_protocol(self):
        try:
            temp = self.connection.recv(4096)
            if temp:
                self.buffer += temp
                return len(temp)
            else:
                print('client disconnected')
                self.shutdown()
                return 0
        except ConnectionResetError:
            print('client disconnected')
            self.shutdown()
            return -1


    def prep_send_channel_protocol(self, tag, input):
        if isinstance(input, bytes):
            input = input.decode()
        arr = bytes(input, 'utf-8')
        return struct.pack('>BI', tag, len(input)) + arr

    def shutdown(self):
        self.sel.unregister(self.connection)
        self.connection.close()
